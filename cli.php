<?php

require_once __DIR__.'/vendor/autoload.php'; 

use Detector\Detector;
use Detector\Model\Image;


$image = new Image();
$image->setPath('tmp/croppedimg2.png');
$detector = new Detector();
$collection = $detector->detect($image);
echo json_encode($collection->getRowNumbers());