<?php

require_once __DIR__.'/../vendor/autoload.php'; 

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application(); 

$app->post('/process-image', function(Request $request) use($app) {
	print_r( $request->files->get('image'));
	return 'test';
}); 

$app->run(); 