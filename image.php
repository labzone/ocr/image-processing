<?php

require_once __DIR__.'/vendor/autoload.php';

use ColorThief\ColorThief;


use Detector\Detector;
use Detector\Core\Matrix;
use Detector\Model\Image;

$image = new Image();
$image->setPath('tmp/croppedimg2.png');
$detector = new Detector();
$collection = $detector->extract($image);
 
//Matrix::getInstance()->compare();