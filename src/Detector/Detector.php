<?php

namespace Detector;

use Detector\Model\Image;
use Detector\Core\Tesseract;
use Detector\Core\TextCleaner;
use Detector\Utils\MapTextToModel;
use Detector\Core\Image as ImageManipulator;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Detector\Core\Matrix;
use Imagine\Exception\OutOfBoundsException;

class Detector
{
    /**
     * image.
     *
     * @var Image
     */
    private $image;
    private $imagePath;
    private $tesseract;
    private $cleaner;
    private $matrix;

    public function __construct()
    {
        $this->tesseract = new Tesseract();
        $this->cleaner = new TextCleaner();
        $this->matrix = Matrix::getInstance();
    }

    /**
     * Gets the image.
     *
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image.
     *
     * @param Image $image the image
     *
     * @return self
     */
    public function setImage(Image $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Gets the value of imagePath.
     *
     * @return mixed
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Sets the value of imagePath.
     *
     * @param mixed $imagePath the image path
     *
     * @return self
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function extract(Image $image)
    {
        $width = 90;
        $box = new Box($width, 110);
        $this
            ->cleaner
            ->setImage($image)
            ->process()
        ;
        $j=0;
        $incrementor = $width;
        for ($y=0; $y < 10; $y++) { 
            for ($i=0; $i < 12; $i++) { 
                if($i % 2==0 && $i>0){
                    $j += $incrementor;
                }
                $x = (($i * $incrementor) + $j);
                if($i > 3){
                    $x += 10;
                }
                $path = explode('/', $image->getPath());
                $inc=0;
                switch (end($path)) {
                    case 'crop2.png':
                        $inc = 0;
                        break;
                }
                $point = new Point($x, ($y + $inc) * 100);
                $imageManipulator = new ImageManipulator();
                try{
                    $imageManipulator
                        ->setImage('tmp/output.jpg')
                        ->crop($point, $box)
                        ->resize(13, 14)
                        ->save("resources/crop/{$y}/{$i}.png")
                    ;
                }catch(OutOfBoundsException $e){
                    break 2;
                }
            }
            $j=0;
        }

        return $this;
    }

    public function compare($index = 2)
    {
        $results = [];
        for ($y=0; $y < $index; $y++) { 
            try{
                $results[] = $this
                    ->matrix
                    ->generateIndex(Matrix::INPUT_FILE, Matrix::CROP_DIR."/{$y}", 12)
                    ->compare()
                ;
            }catch(\Exception $e){
                break;
            }
        }

        return implode("\n", $results);
    }

    public function detect(Image $image)
    {
        $this
            ->tesseract
            ->setImage($image)
            ->process()
        ;
        if (!$this->tesseract->getImage()->getContent()) {
            $this
                ->cleaner
                ->setImage($image)
                ->process()
            ;
            $image->setPath('tmp/output.jpg');
            $this
                ->tesseract
                ->setImage($image)
                ->process()
            ;
        }

        if ($content = $this->tesseract->getImage()->getContent()) {
            return MapTextToModel::getModel($content);
        }

        return;
    }
}
