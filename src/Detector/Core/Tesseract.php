<?php

namespace Detector\Core;

use Detector\Model\Image;

class Tesseract extends AbstractProcess
{
    public function getCommand()
    {
        return "tesseract -psm 4 -c tessedit_char_whitelist=0123456789 {$this->image->getPath()} tesseract_output";
    }

    public function process()
    {
        parent::process();
        if ($content = file_get_contents('tesseract_output.txt')) {
            $this->image->setContent($content);
        }

        return $this;
    }
}
