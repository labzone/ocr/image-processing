<?php

namespace Detector\Core;

use Detector\Model\Image;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

abstract class AbstractProcess
{
    protected $image;

    /**
     * Gets the value of image.
     *
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the value of image.
     *
     * @param mixed $image the image
     *
     * @return self
     */
    public function setImage(Image $image)
    {
        $this->image = $image;

        return $this;
    }

    public function process()
    {
        $this->getCommand();
        $process = new Process($this->getCommand());
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }
}
