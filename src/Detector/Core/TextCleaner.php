<?php

namespace Detector\Core;

class TextCleaner extends AbstractProcess
{
    public function getCommand()
    {
        return "bin/textcleaner -g -e normalize -o 15 -f 30 -s 2 -u -T {$this->image->getPath()} tmp/output.jpg";
    }
}
