<?php

namespace Detector\Core;

interface IProcess
{
    public function setImage(Image $image);
    public function getCommand();
    public function process();
}
