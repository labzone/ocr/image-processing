<?php

namespace Detector\Core;

use ColorThief\ColorThief;
use SplFileInfo;

class Matrix
{
    const DATA_FILE = 'resources/data.json';
    const INPUT_FILE = 'resources/input.json';
    const OUTPUT_DIR = 'images';
    const CROP_DIR = 'crop';

    /**
     * matrix indexes.
     *
     * @var array
     */
    private $indexes = array();

    /**
     * matrix instance.
     *
     * @var self
     */
    private static $instance;

    private function __construct()
    {
        $this->indexes = $this->getData();
    }

    private function __clone()
    {
    }

    /**
     * Gets the matrix indexes.
     *
     * @return array
     */
    public function getIndexes()
    {
        return $this->indexes;
    }

    /**
     * Sets the matrix indexes.
     *
     * @param array $indexes the indexes
     *
     * @return self
     */
    public function setIndexes(array $indexes)
    {
        $this->indexes = $indexes;

        return $this;
    }

    public function generateIndex($outputFilename, $outputDirectory = 'images', $index = 10)
    {
        $indexes = [];
        for ($i = 0; $i < $index; ++$i) {
            $indexes[$i] = $this->generate($i, $outputDirectory);
        }
        file_put_contents($outputFilename, json_encode($indexes));

        return $this;
    }

    public function compare()
    {
        $results = [];
        $content = file_get_contents(Matrix::INPUT_FILE);
        $numbers = json_decode($content);
        $distance = [];
        foreach ($numbers as $index => $number) {
            foreach ($this->indexes as $i => $value) {
                $distance[] = levenshtein($number, $value);
            }
            asort($distance);
            $results[] = key($distance);
            $distance = [];
        }

        $output = null;
        foreach ($results as $key => $result) {
            $output .= $result;
            if($key % 2){
                $output .= ' ';
            }
        }

        return $output;
    }

    private function getData()
    {
        if ($this->isFileExist(self::DATA_FILE)) {
            $file = new SplFileInfo(self::DATA_FILE);
            if ($file->getSize() > 0) {
                return $this->getContent(self::DATA_FILE);
            }
        }

        return $this->generateIndex(self::DATA_FILE)->getData();
    }

    private function getContent($filename)
    {
        $data = file_get_contents($filename);

        return json_decode($data);
    }

    private function isFileExist($filename)
    {
        return file_exists($filename);
    }

    private function generate($number, $outputDirectory = 'images')
    {
        $text = null;
        $image = "resources/{$outputDirectory}/{$number}.png";
        if(!$this->isFileExist($image)){
            throw new \Exception("File not found");
        }
        list($width, $height) = getimagesize($image);
        for ($y = 0; $y < ceil($height); ++$y) {
            for ($x = 0; $x < ceil($width); ++$x) {
                $area = [
                    'x' => $x,
                    'y' => $y,
                    'w' => 1,
                    'h' => 1,
                ];
                try {
                    $dominantColor = ColorThief::getColor($image, 10, $area);
                    list($red, $blue, $green) = $dominantColor;
                    if ($red < 100) {
                        $text .= '1';
                    } else {
                        $text .= '0';
                    }
                } catch (\RuntimeException $e) {
                    $text .= '0';
                }
            }
            $text .= "\n";
        }

        return $text;
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Self();
        }

        return self::$instance;
    }
}
