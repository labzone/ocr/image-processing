<?php

namespace Detector\Core;

use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

class Image
{
    private $image;

    public function setImage($path)
    {
        $imagine = new Imagine();
        $this->image = $imagine->open($path);

        return $this;
    }

    public function grayscale()
    {
        $this->image->effects()->grayscale();

        return $this;
    }

    public function resize($width, $height)
    {
        $this->image->resize(new Box($width, $height));

        return $this;
    }

    public function crop(Point $point, Box $box)
    {
        $this->image->crop($point, $box);

        return $this;
    }

    public function gamma($value)
    {
        $this->image->effects()->gamma($value);

        return $this;
    }

    public function save($path)
    {
        $this->image->save($path);

        return $this;
    }
}
