<?php

namespace Detector\Utils;

use Detector\Model\TextRow;
use Detector\Model\TextRowCollection;

class MapTextToModel
{
    public static function getModel($text)
    {
        $collection = new TextRowCollection();
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $text) as $key => $line) {
            if (strlen($line) > 0) {
                self::mapLineToCollection($line, $collection);
            }
        }

        return $collection;
    }

    private static function mapLineToCollection($line, TextRowCollection $collection)
    {
        for ($i = 0; $i < strlen($line); ++$i) {
            $number = $line[$i];
            if (!preg_match('/\d/', $number)) {
                continue;
            }
            if (!$collection->hasTextRow($i)) {
                $collection->addTextRow(new TextRow(), $i);
            }
            $collection
                ->getTextRowByIndex($i)
                ->addNumber($number)
            ;
        }
    }
}
