<?php

namespace Detector\Model;

class Image
{
    /**
     * image content.
     *
     * @var string
     */
    private $content;

    /**
     * text read from image content.
     *
     * @var string
     */
    private $text;

    private $path;

    /**
     * text row collection.
     *
     * @var TextRowCollection
     */
    private $textRowCollection;

    public function __construct()
    {
        $this->textRowCollection = new TextRowCollection();
    }

    /**
     * Gets the image content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the image content.
     *
     * @param string $content the content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Gets the text read from image content.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text read from image content.
     *
     * @param string $text the text
     *
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Gets the text row collection.
     *
     * @return TextRowCollection
     */
    public function getTextRowCollection()
    {
        return $this->textRowCollection;
    }

    /**
     * Sets the text row collection.
     *
     * @param TextRowCollection $textRowCollection the text row collection
     *
     * @return self
     */
    public function setTextRowCollection(TextRowCollection $textRowCollection)
    {
        $this->textRowCollection = $textRowCollection;

        return $this;
    }

    public function __toString()
    {
        return $this->textRowCollection;
    }

    /**
     * Gets the value of path.
     *
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets the value of path.
     *
     * @param mixed $path the path
     *
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }
}
