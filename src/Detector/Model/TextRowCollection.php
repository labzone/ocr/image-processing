<?php

namespace Detector\Model;

class TextRowCollection
{
    /**
     * text row array.
     *
     * @var array
     */
    private $textRows = array();

    /**
     * Gets the text row array.
     *
     * @return array
     */
    public function getTextRows()
    {
        return $this->textRows;
    }

    public function hasTextRow($index)
    {
        return isset($this->textRows[$index]);
    }

    public function getTextRowByIndex($index)
    {
        return $this->textRows[$index];
    }

    /**
     * Sets the text row array.
     *
     * @param array $textRows the text rows
     *
     * @return self
     */
    public function setTextRows(array $textRows)
    {
        $this->textRows = $textRows;

        return $this;
    }

    /**
     * add text row.
     *
     * @param TextRow $textRow
     */
    public function addTextRow(TextRow $textRow, $index = null)
    {
        if (is_int($index)) {
            $this->textRows[$index] = $textRow;
        } else {
            $this->textRows[] = $textRow;
        }

        return $this;
    }

    public function getRowNumbers()
    {
        $numbers = [];
        foreach ($this->textRows as $key => $textRow) {
            $numbers[] = $textRow->getNumbers();
        }

        return $numbers;
    }

    public function __toString()
    {
        $string = null;
        foreach ($this->textRows as $textRow) {
            $string .= $textRow;
        }

        return $string;
    }
}
