<?php

namespace Detector\Model;

class TextRow
{
    /**
     * numbers.
     *
     * @var array
     */
    private $numbers = array();

    /**
     * Gets the numbers.
     *
     * @return array
     */
    public function getNumbers()
    {
        return $this->numbers;
    }

    /**
     * Sets the numbers.
     *
     * @param array $numbers the numbers
     *
     * @return self
     */
    public function setNumbers(array $numbers)
    {
        $this->numbers = ($numbers);

        return $this;
    }

    /**
     * add number.
     *
     * @param int $number
     */
    public function addNumber($number)
    {
        array_unshift($this->numbers, $number);

        return $this;
    }

    public function __toString()
    {
        $string = null;
        foreach ($this->numbers as $number) {
            $string .= $number;
        }

        return $string;
    }
}
