<?php

require_once __DIR__.'/vendor/autoload.php';

use Detector\Detector;
use Detector\Model\Image;

$image = new Image();
$image->setPath('tmp/TestImages/cropclear2.png');
$detector = new Detector();
$output = $detector->extract($image)->compare(4);
file_put_contents('out.txt', $output."\n");
echo "\n{$output}\n\n";